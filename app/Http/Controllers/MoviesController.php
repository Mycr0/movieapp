<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    public function listQualityMovies()
    {
        return Movie::visible(config('const.movie.status.visible'))->rating(5)->get();
    }

    public function deleteMovie($movieId)
    {
        $movie = Movie::findOrFail($movieId);

        if($movie) $movie->delete();
    }

    public function addMovie(Request $request)
    {
        if ($request->isMethod('post')) {

            $movie = new Movie();

            if(isset($request->name)) $movie->name = $request->name;
            if(isset($request->rating)) $movie->rating = $request->rating;
            if(isset($request->description)) $movie->description = $request->description;
            if(isset($request->image)) $movie->image = $request->image->getClientOriginalName();

            $movie->save();

            return $movie;
        }
    }

    public function listMovies()
    {
        $arrMovies = [];
        $movies = Movie::visible(config('const.movie.status.visible'))->get();

        foreach($movies as $movie){
            $arrMovies[$movie->id]['image'] = $movie->image;
            $arrMovies[$movie->id]['name'] = $movie->name;
            $arrMovies[$movie->id]['rating'] = $movie->rating;
            $arrMovies[$movie->id]['description'] = $movie->description;

            foreach($movie->artists->groupBy('title') as $title => $artist){
                $arrMovies[$movie->id]['artists'][$title][] = $artist;
            }
        }

        return view('app', compact('arrMovies'));
    }
}
