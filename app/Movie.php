<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    protected $table = 'movies';
    use SoftDeletes;

    public function artists()
    {
        return $this->belongsToMany(Artist::class, 'movie_artist');
    }

    public function scopeVisible($query, $value)
    {
        return $query->where('status', $value);
    }

    public function scopeRating($query, $value)
    {
        return $query->where('rating', '>', $value);
    }

}
