<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Artist extends Model
{
    use SoftDeletes;

    protected $table = 'artists';

    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'movie_artist');
    }

    public function scopeTitle($query, $value)
    {
        return $query->where('title', 'like', $value);
    }
}
