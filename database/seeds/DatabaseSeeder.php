<?php

use Illuminate\Database\Seeder;
use App\Movie;
use App\Artist;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(Movie::class, 3)->create();
        factory(Artist::class, 7)->create();

        $movie = App\Movie::all();

        // Populate the pivot table
        App\Artist::all()->each(function ($artist) use ($movie) {
            $artist->movies()->attach(
                $movie->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
    }
}
