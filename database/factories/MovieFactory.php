<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Movie::class, function (Faker $faker) {
    return [
        'status' => 1,
        'name' => $faker->name,
        'rating' => $faker->numberBetween(1,10),
        'description' => $faker->text(120),
        'image' => $faker->randomElement(['forest-gump.jpg', 'joker.jpg', 'space-force.jpg'])
    ];
});
