<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/movies/', 'MoviesController@listQualityMovies')->name('list_quality_movies');
Route::get('/movies/{id}', 'MoviesController@deleteMovie')->name('delete_movie');
Route::post('/movies/', 'MoviesController@addMovie')->name('add_movies');

