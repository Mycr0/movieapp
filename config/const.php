<?php

/**
 * Description: applicatins constants
 */

return [
    'movie' => [
        // question types
        'status' => [
            'visible' => 1, // single answer per row
            'hidden' => 2, // multiple answers per row
        ],
    ]
];
