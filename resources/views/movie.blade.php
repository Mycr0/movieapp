
<div class="row mb-3">
    <div class="col-md-12">
        <div class="card flex-row flex-wrap">
            <img class="py-1 pl-1 pb-1" src="{{asset('images/movies/')}}/{{$movie['image']}}" alt="">

            <div class="card-block px-2 py-1">
                <div>
                    <h4 class="card-title"><strong>{{$movie['name']}}</strong></h4>
                    <div class="d-flex">
                        <img id="star-imdb" src="{{asset('images/app/star-solid.svg')}}" alt="">
                        <span  class="rating pl-1">{{$movie['rating']}}</span>
                    </div>
                </div>

                @if(isset($movie['artists']))
                    <div class="movie-artists">
                        @foreach($movie['artists'] as $titles=>$artists)
                        <div>
                            <span class="card-text font-weight-bold">{{$titles}}</span>:
                            <span>
                            @foreach($artists[0] as $artist)
                               {{ $artist['name']}}{{$loop->last ? '' : ', '}}
                            @endforeach
                            </span>
                        </div>
                        @endforeach
                    </div>
                @endif

                <div class='movie-description'>
                    <span>{{$movie['description']}}</span>
                </div>

                <a href="#" class="btn btn-yellow-imdb">View more</a>
            </div>
            <div class="w-100"></div>
        </div>
    </div>
</div>
