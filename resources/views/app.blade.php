<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Movie App</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,700;1,600&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">

    </head>
    <body>
    <nav  class="navbar navbar-expand-xs navbar-light bg-yellow-imdb">
        <button onclick="openNav()" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <a href="#"><img id='logo' src="{{asset('images/app/logo-imdb.png')}}"></a>

        <img id='user_account' src="{{asset('images/app/user-solid.svg')}}">

        <!-- The overlay -->
        <div id="myNav" class="overlay">

            <!-- Button to close the overlay navigation -->
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="fa fa-arrow-left fa-xs"></i></a>

            <!-- Overlay content -->
            <div class="overlay-content">
                <a href="#">This is a full screen menu</a>
                <a href="#">Close me from the upper "Back arrow"</a>
            </div>

        </div>

    </nav>

    <div class="container-fluid mt-5">
        @foreach($arrMovies as $movie)
            @include('movie')
        @endforeach
    </div>
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{asset('js/app.js')}}"></script>
</html>
